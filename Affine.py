import random, math, sys

charMap = """ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`~!@#$%^&*()-_=+[\]{|}"':;<>,./?"""
mapLen = len(charMap);


def main():
    modeSelect = input("Enter E to encrypt a message, Enter D to decrypt a message: ")
    if(modeSelect == "E" or modeSelect == "e"):
        fileName = input("Enter the file you would like to encrypt: ")
        plainFile = open(fileName, "r")
        plainText = plainFile.read()

        keyA, keyB = randKeyGen()
        cipherText = encryptMessage(keyA, keyB, plainText)

        cipherFile = open("cipherText.txt", "w+")
        cipherFile.write(cipherText)
        cipherFile.close()

        print("KeyA: " + str(keyA) + " KeyB: " + str(keyB))
        print("Your file has been encrypted in cipherText.txt")
    if(modeSelect == "D" or modeSelect == "d"):
        fileName = input("Enter the file you would like to decrypt: ")
        cipherFile = open(fileName, "r")
        cipherText = cipherFile.read()

        keyA = int(input("Enter keyA used for encryption: "))
        keyB = int(input("Enter keyB used for encryption: "))

        decryptText = decryptMessage(keyA, keyB, cipherText)
        decryptFile = open("decrypt.txt", "w+")
        decryptFile.write(decryptText)
        decryptFile.close()

        print("Your file has been decrypted in decrypt.txt")
    
def encryptMessage(keyA, keyB, plainText):
    cipherText = ""
    for plainChar in plainText:
        plainIndex = charMap.find(plainChar)
        cipherIndex = (plainIndex * keyA + keyB) % mapLen
        cipherText += charMap[cipherIndex]
    return cipherText

def decryptMessage(keyA, keyB, cipherText):
    plainText = ""
    invOfA = findAInv(keyA)
    for cipherChar in cipherText:
        cipherIndex = charMap.find(cipherChar)
        plainIndex = invOfA * (cipherIndex - keyB) % mapLen
        plainText += charMap[plainIndex]
    return plainText

def findAInv(keyA):
    keyA = keyA % mapLen
    for x in range (1, mapLen):
        if((keyA * x) % mapLen == 1):
            return x
    return -1

def randKeyGen():
    while True:
        keyA = random.randint(1, mapLen)
        keyB = random.randint(1, mapLen)
        if(math.gcd(keyA, mapLen) == 1):
            return keyA, keyB

if __name__ == '__main__':
    main()